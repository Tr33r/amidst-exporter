AMIDST Exporter
===============

This is a version of Amidst with an Export menu added to it, intended for use with the Ink & Parchment map system maintained 
at [buildwithblocks.info](http://buildwithblocks.info). The compiled/executable version can be [downloaded from here](http://www.buildingwithblocks.info/exportfromseed.html).

With it, players can automatically generate location list files and ocean-maps (allowing [ocean backgrounds such as this](http://www.buildingwithblocks.info/images/ocean_preview_small.png)) with nothing more than the world's seed.

The original AMIDST can be found [here](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-tools/1262200-v3-6-amidst-strongholds-village-biome-etc-finder).

Requirements
------------
800 MB of RAM - This is needed if you wish to export, as a large area of the Minecraft map is generated before exporting the locations and oceans. So, if running from the command line, try:

    java -Xmx1024M -jar ./AmidstExporter.jar


License and warranty
--------------------

AMIDST comes with ABSOLUTELY NO WARRANTY. It is free software, and you are
welcome to redistribute it under certain conditions. See LICENSE.txt for more
details on both of these points.
